<?php
include 'functions.php';
include 'pxpay/Pxpay.php';
/**
 * 下订单,支付页面查询订单,获取订单支付状态
 */

$act = $_GET['act'];
if($act == 'new'){
    $paytype = $_GET['paytype'];
    $money_value = isset($_GET['money'])?$_GET['money']:0.01;
    if(is_string($money_value)){
        $money_value = (float)$money_value;
    }
    if($money_value<0.01){
        $money_value = 0.01;
    }
    if($money_value>1){
        $money_value = 1;
    }
    if ($paytype != 'ALIPAY' && $paytype != 'WXPAY') {
        die('未选择支付方式');
    } else {
        $orderid = time(); //自己的订单ID,不可以重复
        $money = $money_value; //订单金额,必须有对应的二维码
        $args = [
            'money' => $money,
            'orderid' => $orderid,
            'paytype' => $paytype,
            'notify_url' => getMyUrl() . '/notify.php',
            'return_url' => getMyUrl() . '/complete.html',
        ];
        $pxpay = new pxpay\Pxpay();
        //$pxpay->init('appid','secret');//输入自己的appid,secret
        $rs = $pxpay->addOrder($args);
        if($rs){
              //跳转到支付页面
            setOrder($rs);//这里存在文本文件中,实际应该存入数据库.
            $url = 'pay.html?orderid=' . $rs['orderid'];
            //支付链接,打开这个链接就可以支付了.返回给客户端,或者自己直接跳转
            //如果通过ajax请求本接口.就返回url,客户端拿到支付链接后,打开即可;
            header("Location: $url"); 
        }else{
            echo $pxpay->getErrorMsg();
        }
        exit();
    }
}
$orderid = $_GET['orderid'];
if($act == 'checkorder'){
	//检测订单状态
	$status = getOrder($orderid);
	$data = [
		'pay'=>isset($status['status'])?$status['status']:0,
		'url'=>'complate.html'
	];
}else{
	//支付页面，获取订单信息
	$data = getOrder($orderid);//这里从文本中加载.实际应该从数据库里读取
}

echo json(0,$data);